USE [master]
GO
/****** Object:  Database [whatflix]    Script Date: 2/16/2019 8:09:28 AM ******/
CREATE DATABASE [whatflix]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'whatflix', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\whatflix.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'whatflix_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\whatflix_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [whatflix] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [whatflix].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [whatflix] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [whatflix] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [whatflix] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [whatflix] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [whatflix] SET ARITHABORT OFF 
GO
ALTER DATABASE [whatflix] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [whatflix] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [whatflix] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [whatflix] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [whatflix] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [whatflix] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [whatflix] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [whatflix] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [whatflix] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [whatflix] SET  DISABLE_BROKER 
GO
ALTER DATABASE [whatflix] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [whatflix] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [whatflix] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [whatflix] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [whatflix] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [whatflix] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [whatflix] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [whatflix] SET RECOVERY FULL 
GO
ALTER DATABASE [whatflix] SET  MULTI_USER 
GO
ALTER DATABASE [whatflix] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [whatflix] SET DB_CHAINING OFF 
GO
ALTER DATABASE [whatflix] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [whatflix] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [whatflix] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'whatflix', N'ON'
GO
ALTER DATABASE [whatflix] SET QUERY_STORE = OFF
GO
USE [whatflix]
GO
/****** Object:  Table [dbo].[Actors]    Script Date: 2/16/2019 8:09:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Actors](
	[actor_id] [bigint] NOT NULL,
	[name] [nvarchar](50) NOT NULL,
	[sex] [nvarchar](50) NOT NULL,
	[Bio] [ntext] NULL,
	[DOB] [datetime] NOT NULL,
 CONSTRAINT [pk_key] PRIMARY KEY CLUSTERED 
(
	[actor_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[movie_credits]    Script Date: 2/16/2019 8:09:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[movie_credits](
	[movie_id] [bigint] NOT NULL,
	[producer_id] [bigint] NOT NULL,
	[actor_id] [bigint] NOT NULL,
 CONSTRAINT [pk_2k] PRIMARY KEY CLUSTERED 
(
	[movie_id] ASC,
	[producer_id] ASC,
	[actor_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[movies]    Script Date: 2/16/2019 8:09:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[movies](
	[movie_id] [bigint] NOT NULL,
	[name] [nvarchar](50) NOT NULL,
	[yearofrelease] [datetime] NOT NULL,
	[plot] [ntext] NULL,
	[poster] [nvarchar](50) NULL,
	[producer_id] [bigint] NOT NULL,
 CONSTRAINT [pk_k] PRIMARY KEY CLUSTERED 
(
	[movie_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Producers]    Script Date: 2/16/2019 8:09:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Producers](
	[producer_id] [bigint] NOT NULL,
	[DOB] [datetime] NOT NULL,
	[Bio] [ntext] NULL,
	[Sex] [nvarchar](50) NOT NULL,
	[name] [nvarchar](50) NOT NULL,
 CONSTRAINT [pk_producers] PRIMARY KEY CLUSTERED 
(
	[producer_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[movie_credits]  WITH CHECK ADD  CONSTRAINT [fk_actors_id] FOREIGN KEY([actor_id])
REFERENCES [dbo].[Actors] ([actor_id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[movie_credits] CHECK CONSTRAINT [fk_actors_id]
GO
ALTER TABLE [dbo].[movie_credits]  WITH CHECK ADD  CONSTRAINT [fk_producers_id] FOREIGN KEY([producer_id])
REFERENCES [dbo].[Producers] ([producer_id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[movie_credits] CHECK CONSTRAINT [fk_producers_id]
GO
ALTER TABLE [dbo].[movies]  WITH CHECK ADD  CONSTRAINT [fk_producer_id] FOREIGN KEY([producer_id])
REFERENCES [dbo].[Producers] ([producer_id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[movies] CHECK CONSTRAINT [fk_producer_id]
GO
USE [master]
GO
ALTER DATABASE [whatflix] SET  READ_WRITE 
GO
