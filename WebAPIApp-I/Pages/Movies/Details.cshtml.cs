﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using WebAPIApp_I.Models;

namespace WebAPIApp_I.Pages.Movies
{
    public class DetailsModel : PageModel
    {
        private readonly WebAPIApp_I.Models.WebAPIApp_IContext _context;

        public DetailsModel(WebAPIApp_I.Models.WebAPIApp_IContext context)
        {
            _context = context;
        }

        public WebAPIApp_I.Models.Movies Movies { get; set; }

        public async Task<IActionResult> OnGetAsync(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Movies = await _context.Movies
                .Include(m => m.Producer).FirstOrDefaultAsync(m => m.MovieId == id);

            if (Movies == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
