﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using WebAPIApp_I.Models;

namespace WebAPIApp_I.Pages.Movies
{
    public class IndexModel : PageModel
    {
        private readonly WebAPIApp_I.Models.WebAPIApp_IContext _context;

        public IndexModel(WebAPIApp_I.Models.WebAPIApp_IContext context)
        {
            _context = context;
        }

        public IList<WebAPIApp_I.Models.Movies> Movies { get;set; }

        public async Task OnGetAsync()
        {
            Movies = await _context.Movies
                .Include(m => m.Producer).ToListAsync();
        }
    }
}
