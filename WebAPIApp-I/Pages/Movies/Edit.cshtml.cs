﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using WebAPIApp_I.Models;

namespace WebAPIApp_I.Pages.Movies
{
    public class EditModel : PageModel
    {
        private readonly WebAPIApp_I.Models.WebAPIApp_IContext _context;

        public EditModel(WebAPIApp_I.Models.WebAPIApp_IContext context)
        {
            _context = context;
        }

        [BindProperty]
        public WebAPIApp_I.Models.Movies Movies { get; set; }

        public async Task<IActionResult> OnGetAsync(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Movies = await _context.Movies
                .Include(m => m.Producer).FirstOrDefaultAsync(m => m.MovieId == id);

            if (Movies == null)
            {
                return NotFound();
            }
           ViewData["ProducerId"] = new SelectList(_context.Producers, "ProducerId", "Name");
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Attach(Movies).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MoviesExists(Movies.MovieId))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./Index");
        }

        private bool MoviesExists(long id)
        {
            return _context.Movies.Any(e => e.MovieId == id);
        }
    }
}
