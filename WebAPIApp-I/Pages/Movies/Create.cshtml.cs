﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using WebAPIApp_I.Models;

namespace WebAPIApp_I.Pages.Movies
{
    public class CreateModel : PageModel
    {
        private readonly WebAPIApp_I.Models.WebAPIApp_IContext _context;

        public CreateModel(WebAPIApp_I.Models.WebAPIApp_IContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
        ViewData["ProducerId"] = new SelectList(_context.Producers, "ProducerId", "Name");
            return Page();
        }

        [BindProperty]
        public WebAPIApp_I.Models.Movies Movies { get; set; }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Movies.Add(Movies);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}