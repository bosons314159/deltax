﻿using System;
using System.Collections.Generic;

namespace WebAPIApp_I.Models
{
    public partial class Producers
    {
        public Producers()
        {
            MovieCredits = new HashSet<MovieCredits>();
            Movies = new HashSet<Movies>();
        }

        public long ProducerId { get; set; }
        public DateTime Dob { get; set; }
        public string Bio { get; set; }
        public string Sex { get; set; }
        public string Name { get; set; }

        public virtual ICollection<MovieCredits> MovieCredits { get; set; }
        public virtual ICollection<Movies> Movies { get; set; }
    }
}
