﻿using System;
using System.Collections.Generic;

namespace WebAPIApp_I.Models
{
    public partial class MovieCredits
    {
        public long MovieId { get; set; }
        public long ProducerId { get; set; }
        public long ActorId { get; set; }

        public virtual Actors Actor { get; set; }
        public virtual Producers Producer { get; set; }
    }
}
