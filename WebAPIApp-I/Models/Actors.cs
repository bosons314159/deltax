﻿using System;
using System.Collections.Generic;

namespace WebAPIApp_I.Models
{
    public partial class Actors
    {
        public Actors()
        {
            MovieCredits = new HashSet<MovieCredits>();
        }

        public long ActorId { get; set; }
        public string Name { get; set; }
        public string Sex { get; set; }
        public string Bio { get; set; }
        public DateTime Dob { get; set; }

        public virtual ICollection<MovieCredits> MovieCredits { get; set; }
    }
}
