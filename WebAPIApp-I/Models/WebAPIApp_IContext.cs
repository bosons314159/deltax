﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace WebAPIApp_I.Models
{
    public partial class WebAPIApp_IContext : DbContext
    {
        public WebAPIApp_IContext()
        {
        }

        public WebAPIApp_IContext(DbContextOptions<WebAPIApp_IContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Actors> Actors { get; set; }
        public virtual DbSet<MovieCredits> MovieCredits { get; set; }
        public virtual DbSet<Movies> Movies { get; set; }
        public virtual DbSet<Producers> Producers { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Server=DESKTOP-HL0L2KE;Database=whatflix;Trusted_Connection=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.2-servicing-10034");

            modelBuilder.Entity<Actors>(entity =>
            {
                entity.HasKey(e => e.ActorId)
                    .HasName("pk_key");

                entity.Property(e => e.ActorId)
                    .HasColumnName("actor_id")
                    .ValueGeneratedNever();

                entity.Property(e => e.Bio).HasColumnType("ntext");

                entity.Property(e => e.Dob)
                    .HasColumnName("DOB")
                    .HasColumnType("datetime");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(50);

                entity.Property(e => e.Sex)
                    .IsRequired()
                    .HasColumnName("sex")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<MovieCredits>(entity =>
            {
                entity.HasKey(e => new { e.MovieId, e.ProducerId, e.ActorId })
                    .HasName("pk_2k");

                entity.ToTable("movie_credits");

                entity.Property(e => e.MovieId).HasColumnName("movie_id");

                entity.Property(e => e.ProducerId).HasColumnName("producer_id");

                entity.Property(e => e.ActorId).HasColumnName("actor_id");

                entity.HasOne(d => d.Actor)
                    .WithMany(p => p.MovieCredits)
                    .HasForeignKey(d => d.ActorId)
                    .HasConstraintName("fk_actors_id");

                entity.HasOne(d => d.Producer)
                    .WithMany(p => p.MovieCredits)
                    .HasForeignKey(d => d.ProducerId)
                    .HasConstraintName("fk_producers_id");
            });

            modelBuilder.Entity<Movies>(entity =>
            {
                entity.HasKey(e => e.MovieId)
                    .HasName("pk_k");

                entity.ToTable("movies");

                entity.Property(e => e.MovieId)
                    .HasColumnName("movie_id")
                    .ValueGeneratedNever();

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(50);

                entity.Property(e => e.Plot)
                    .HasColumnName("plot")
                    .HasColumnType("ntext");

                entity.Property(e => e.Poster)
                    .HasColumnName("poster")
                    .HasMaxLength(50);

                entity.Property(e => e.ProducerId).HasColumnName("producer_id");

                entity.Property(e => e.Yearofrelease)
                    .HasColumnName("yearofrelease")
                    .HasColumnType("datetime");

                entity.HasOne(d => d.Producer)
                    .WithMany(p => p.Movies)
                    .HasForeignKey(d => d.ProducerId)
                    .HasConstraintName("fk_producer_id");
            });

            modelBuilder.Entity<Producers>(entity =>
            {
                entity.HasKey(e => e.ProducerId)
                    .HasName("pk_producers");

                entity.Property(e => e.ProducerId)
                    .HasColumnName("producer_id")
                    .ValueGeneratedNever();

                entity.Property(e => e.Bio).HasColumnType("ntext");

                entity.Property(e => e.Dob)
                    .HasColumnName("DOB")
                    .HasColumnType("datetime");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(50);

                entity.Property(e => e.Sex)
                    .IsRequired()
                    .HasMaxLength(50);
            });
        }
    }
}
