﻿using System;
using System.Collections.Generic;

namespace WebAPIApp_I.Models
{
    public partial class Movies
    {
        public long MovieId { get; set; }
        public string Name { get; set; }
        public DateTime Yearofrelease { get; set; }
        public string Plot { get; set; }
        public string Poster { get; set; }
        public long ProducerId { get; set; }

        public virtual Producers Producer { get; set; }
    }
}
